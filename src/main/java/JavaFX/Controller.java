package JavaFX;

import Connectivity.ConnectionClass;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;


public class Controller implements Initializable {


    @FXML
    private Label lbl_close;

    @FXML
    private JFXTextField txtUsername;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Label isConnected;


    public void handleButtonAction(MouseEvent event) {
        if (event.getSource() == lbl_close)
            System.exit(0);
    }


    public void login(ActionEvent actionEvent) {
        ConnectionClass connectionClass = new ConnectionClass();
        Connection connection = connectionClass.getConnection();

        try {
            Statement statement = connection.createStatement();
            String sql = "SELECT FROM userAuth WHERE username ='"+txtUsername.getText()+"' AND password ='"+txtPassword.getText()+"'";
            ResultSet resultSet = statement.executeQuery(sql);

            if(resultSet.next()){
                isConnected.setText("Connected");
            }
            else{
                isConnected.setText("not Conencted");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void initialize(URL location, ResourceBundle resources) {

    }


}
