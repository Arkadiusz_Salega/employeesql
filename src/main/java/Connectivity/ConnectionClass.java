package Connectivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.TimeZone;

public class ConnectionClass {


    public Connection connection;

    public Connection getConnection() {


        String dbName = "jdbc:mysql://localhost:3306/tutorial";
        String userName = "root";
        String password = "admin123";


        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            connection = DriverManager.getConnection(dbName, userName, password + TimeZone.getDefault().getID());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return connection;
    }


}
